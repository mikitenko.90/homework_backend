package workSimpleLevel;

import java.util.Scanner;

public class Task1 {

	private static Scanner sc;

	public static void main(String[] args) {
		sc = new Scanner(System.in);
		int num1;
		int num2;
		int num3;
		int num4;
		int maxNum;
		
		System.out.println("Input first number");
		num1 = sc.nextInt();
		System.out.println("Input second number");
		num2 = sc.nextInt();	
		System.out.println("Input third number");
		num3 = sc.nextInt();
		System.out.println("Input fourth number");
		num4 = sc.nextInt();
		maxNum = num1;
		if(maxNum<num2) {
			maxNum=num2;
		}
		if(maxNum<num3) {
			maxNum=num3;
		}
		if(maxNum<num4) {
			maxNum=num4;
		}
		System.out.println("Max Number is " + maxNum);
	}

}

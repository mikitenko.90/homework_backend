package sample;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("input string ");
		String line = sc.nextLine();
		char[] symbols = line.toCharArray();
		int count = 0;
		for (int i = 0; i < symbols.length; i++) {
			if (symbols[i] == 'b' || symbols[i] == 'B') {
				count++;
			}
		}
		System.out.print(count);
	}
}

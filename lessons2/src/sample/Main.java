package sample;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Input coffee cost (UAH)");
    int coffeCost = sc.nextInt();
    System.out.println("Кількість товару: ");
    int cups = sc.nextInt();
    int totalSum;

    totalSum = coffeCost * cups;
    System.out.println(totalSum + " UAH");
  }

}
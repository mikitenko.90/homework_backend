package homeWorkOOP3;

import java.util.Arrays;

public class Group {
	private	String groupName;
	private final Student[] studens = new Student[10];
	
	
	public Group(String groupName) {
		super();
		this.groupName = groupName;
	}
	public Group() {

	}

	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Student[] getStudens() {
		return studens;
	}
	
	public void addStudent(Student student) throws GroupOverflowException  {
		for(int i=0; i<studens.length; i++){
			if(studens[i] == null ) {
				studens[i]=student;
				return;
				}			
		}

		throw new GroupOverflowException("Group is full");
	}
	
	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException{
		for(int i=0;i<studens.length;i++) {
		if(studens[i]!=null && studens[i].getLastName().equals(lastName))
		return studens[i];
		}
		throw new StudentNotFoundException("Not found");
	}
	
	public boolean removeStudentByID(int id){
		for(int i=0;i<studens.length;i++) {
			if(studens[i]!=null && studens[i].getId() == id) {
				studens[i] = null;
				return true;
				}
			}
		return false;
		
	}
	

	@Override
	public String toString() {
		return "Group [groupName=" + groupName + ", studens=" + Arrays.toString(studens) + "]";
	}
	
	}

package homeWorkOOP3;

public class main {
	
	public static void main(String[] args) {
		
		Student first = new Student("Igor", "Mykytenko", Gender.Maile, 1, "OOP");
		Student second = new Student("Oleksandr", "Ponomarenko", Gender.Maile,2 , "OOP");
		Student tr = new Student("Vitaliy", "Ponomarenko", Gender.Maile,3 , "OOP");
		Student four = new Student("Oksana", "Ponomarenko", Gender.Femaile,4 , "OOP");
		Student fv = new Student("Olga", "Ponomarenko", Gender.Femaile,5 , "OOP");
		Student sx = new Student("Victoria", "Ponomarenko", Gender.Femaile,6 , "OOP");
		Student sev = new Student("Mary", "Ponomarenko", Gender.Femaile,7 , "OOP");
		Student eig = new Student("Maksym", "Ponomarenko", Gender.Maile,8 , "OOP");
		Student nin = new Student("Masha", "Ponomarenko", Gender.Femaile,9 , "OOP");
		Student ten = new Student("Mykola", "Ponomarenko", Gender.Maile,10 , "OOP");
		Student elev = new Student("Ivan", "Ponomarenko", Gender.Maile,11 , "OOP");
		
		
		
		Group one = new Group();
		one.setGroupName("OOP");
		
		try {
		one.addStudent(first);
		one.addStudent(second);
		one.addStudent(tr);
		one.addStudent(four);
		one.addStudent(fv);
		one.addStudent(sx);
		one.addStudent(sev);
		one.addStudent(eig);
		one.addStudent(nin);
		one.addStudent(ten);
		one.addStudent(elev);
		}catch(GroupOverflowException e) {
			System.err.println(e.getMessage());
		}
		
		try {
			System.out.println(one.searchStudentByLastName("Mykyteko"));
		}catch(StudentNotFoundException e) {
			System.err.println(e.getMessage());
		}
		
		
		System.out.println(one.removeStudentByID(1));
		System.out.println(one.removeStudentByID(1));
	}
}


package main;

import java.util.Arrays;
import java.util.Random;

public class Task3 {

	public static void main(String[] args) {
		Random rnd = new Random();
		int[] array = new int [15];
		for(int i=0;i<array.length;i++) {
			array[i]=rnd.nextInt(1,100);
			System.out.print(array[i]+" ");

		}
		
		System.out.println();
		
		int[] array1 = Arrays.copyOf(array, array.length*2);
		for(int i=array.length;i<array1.length;i++) {
				array1[i]=array[i-array.length]*2;		
		}
		
		for(int i=0;i<array1.length;i++) {
			System.out.print(array1[i]+" ");	
	}	
	}

}

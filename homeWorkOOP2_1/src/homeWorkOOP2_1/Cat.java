package homeWorkOOP2_1;

public class Cat extends Animal  {
	private String name;

	public Cat(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getVoise() {
		return  name + " say May";
	}
	
	public void eat() {
		System.out.println("I eat  " + getRation());
		}

		public void sleep(){
		System.out.println(  getName()+" don't need sleep");
		}

	@Override
	public String toString() {
		return "Cat [" + super.toString() + "name=" + name + "]";
	}

	

}

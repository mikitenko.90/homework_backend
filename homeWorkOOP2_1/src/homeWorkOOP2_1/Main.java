package homeWorkOOP2_1;

public class Main {

	public static void main(String[] args) {
		Cat cat = new Cat("Milk", "White", 3, "Barsic");
		System.out.println(cat);
		System.out.println(cat.getVoise());
		cat.eat();
		cat.sleep();
		
		Dog dog = new Dog("Meat", "Black", 12, "Barbos");
		System.out.println(dog);
		System.out.println(dog.getVoise());
		dog.eat();
		dog.sleep();
		
		
		Veterinarian person1 = new Veterinarian("Volodymyr");
		System.out.println(person1);
		person1.treatment(dog);
		person1.treatment(cat);
	}
	
	

}

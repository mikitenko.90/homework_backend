package homeWorkOOP1_2;

public class Triangle {
	private double side1;
	private double side2;
	private double side3;

	
	public Triangle(double a, double b, double c) {
		this.side1 = a;
		this.side2 = b;
		this.side3 = c;
	
	}
	

	public Triangle(){
		super();
	}
	
	

	public double getSide1() {
		return side1;
	}


	public void setSide1(double side1) {
		this.side1 = side1;
	}


	public double getSide2() {
		return side2;
	}


	public void setSide2(double side2) {
		this.side2 = side2;
	}


	public double getSide3() {
		return side3;
	}


	public void setSide3(double side3) {
		this.side3 = side3;
	}


	public String pr() {
		double p = (side1 + side2 + side3)/2;
		double S =Math.sqrt(p*(p-side1)*(p-side2)*(p-side3));
		String roundS = String.format("%.2f", S);
		return roundS;
		}


	@Override
	public String toString() {
		return "Triangle [A=" + side1 + ", B=" + side2 + ", C=" + side3 + ", S=" + pr() + "]";
	}

	
	

}

package work;

public class Task1 {

	public static void main(String[] args) {
		double sideA = 0.3;
		double sideB = 0.4;
		double sideC = 0.5;
		
		double halfPerimeter = (sideA + sideB + sideC)/2;
		double square = Math.sqrt(halfPerimeter*(halfPerimeter - sideA)*(halfPerimeter-sideB)*(halfPerimeter-sideC));
		System.out.println(square);

	}

}

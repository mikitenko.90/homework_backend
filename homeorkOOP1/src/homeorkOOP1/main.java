package homeorkOOP1;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CellPhone phone1 = new CellPhone();
		phone1.setName("Sumsung");
		phone1.setColor("Blue");
		phone1.setPrice(300.5);
		
		CellPhone phone2 = new CellPhone();
		phone2.setName("Iphone");
		phone2.setColor("White");
		phone2.setPrice(601);
		
		CellPhone phone3 = new CellPhone();
		phone3.setName("LG");
		phone3.setColor("White");
		phone3.setPrice(204.99);
		
		System.out.println(phone1);
		System.out.println(phone2);
		System.out.println(phone3);
		
	}

}

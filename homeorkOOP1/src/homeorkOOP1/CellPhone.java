package homeorkOOP1;

public class CellPhone {

	private String name;
	private double price;
	private String Color;
	

	public CellPhone(String name, double price, String color) {
		super();
		this.name = name;
		this.price = price;
		Color = color;
	}



	public  CellPhone() {
		super();
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public String getColor() {
		return Color;
	}



	public void setColor(String color) {
		Color = color;
	}



	@Override
	public String toString() {
		return "CellPhone [name=" + name + ", price=" + price + "$" + ", Color=" + Color + "]";
	}
	
	
}